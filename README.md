<div align="center">

  <h1 align="center">U-task</h1>

</div>
<br />
<br />


## About the project

U-task is a project web app that lets you keep track of your projects and a to-do list for those projects.
U-task can create a project, add tasks to that specific project and assign that task to a specific person. 

## Build
U-task was built using a Django framework for the back-end and CSS for the GUI. 

## Projects 
Once a user successfully creates an account or logged in, the user is taken to their projects page. From here they can opt to "Add a ne project" with a new name, desctription, and the members that will be part of the project. Once a project is created, the project will be displayed on the project dashboard including Project Name, and Number of tasks in that project.
The user can click a project to get to the project details page, where the user can add tasks and veiw tasks with whom they are assigned to. The details page keeps track of all project start dates, due dates, and status. 
![U-task-login.gif](images/U-task-login.gif)
![U-task-dashboard.gif](images/U-task-dashboard.gif)
![U-task-creat-project.gif](images/U-task-creat-project.gif)

## Tasks 
On the Tasks dashboard, the user is able to see all tasks assigned to them and the corrisponding project that the task is linked to. From here they can click the project for more details, make the task complete, or exit back to the Projects page. 
![U-task-new-task.gif](images/U-task-new-task.gif)
![U-task-task-complete.gif](images/U-task-task-complete.gif)

## Front-end
Front-end on this projects was done with Django templates and CSS

## Back-end
The back-end was built with a Django framework. Using a LoginMixin the user is only able to update and manage their own projects and tasks assigned to them. 

## Installation and setup
1. Clone the repository down to your local machine
2. CD into the new project directory
3. Create a new virtual environment using `python -m venv .venv` in your terminal
4. Activate a virtual environment
`source ./.venv/bin/activate`  for macOS
`./.venv/Scripts/Activate.ps1`  for Windows
5. `pip install -r requirements.txt` in your terminal
6. `pip freeze > requirements.txt` in your terminal
7. `python manage.py runserver` in your terminal to run the server
8. type `localhost:8000` into your browser to see the app



