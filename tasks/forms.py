from django.forms.widgets import DateInput
from django.forms import ModelForm
from .models import Task


class DateInput(DateInput):
    input_type = "date"


class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = ["name", "start_date", "due_date", "project", "assignee"]
        widgets = {
            "start_date": DateInput(attrs={"type": "date"}),
            "due_date": DateInput(attrs={"type": "date"}),
        }
