from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from tasks.models import Task
from tasks.forms import TaskForm


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    # fields = ["name", "start_date", "due_date", "project", "assignee"]
    template_name = "tasks/new.html"
    form_class = TaskForm

    def get_success_url(self):
        return reverse_lazy(
            "show_project", kwargs={"pk": self.object.project.id}
        )


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "tasks/list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


# 1st try
class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
